angular.module('reportingApp', [])
    .controller('ScreenshotReportController', function() {
        this.getParent = function(str) {
            var arr = str.split('|');
            str = "";
            for(var i=arr.length-1; i>0; i--) {
                str += arr[i] + " > ";
            }
            return str.slice(0, -3);
        };
        this.getShortDescription = function(str) {
            return str.split('|')[0];
        };
        this.nToBr = function(str) {
            return str.replace(/(?:\r\n|\r|\n)/g, '<br />');
        };
        this.getFilename = function(str) {
            return str.replace(/^.*[\\\/]/, '');
        };
        this.passCount = function() {
            var passCount = 0;
            for(var i in this.results) {
                var result = this.results[i];
                if(result.passed) passCount++;
            }
            return passCount;
        };
        this.pendingCount = function() {
            var pendingCount = 0;
            for(var i in this.results) {
                var result = this.results[i];
                if(result.pending) pendingCount++;
            }
            return pendingCount;
        };
        this.failCount = function() {
            var failCount = 0;
            for(var i in this.results) {
                var result = this.results[i];
                if(!result.passed && !result.pending) failCount++;
            }
            return failCount;
        };
        this.results =
[{"description":"Open the Application|Automation \n","passed":true,"pending":false,"os":"Linux","browser":{"name":"chrome","version":"61.0.3163.100"},"message":"Passed.","trace":"","screenShotFile":"00620042-003c-003b-0061-007f000d001b.png"},{"description":"Registration|Automation For \n","passed":true,"pending":false,"os":"Linux","browser":{"name":"chrome","version":"61.0.3163.100"},"message":"Passed.","trace":"","screenShotFile":"00a900d6-001d-0037-00c1-004c008b004e.png"},{"description":"Login|Automation For \n","passed":true,"pending":false,"os":"Linux","browser":{"name":"chrome","version":"61.0.3163.100"},"message":"Passed","screenShotFile":"002600f9-00e6-00a1-0000-005700b90038.png"},{"description":" User Delete|Automation For \n","passed":true,"pending":false,"os":"Linux","browser":{"name":"chrome","version":"61.0.3163.100"},"message":"Passed","screenShotFile":"0058009e-00b3-00a7-008b-00e9008300f2.png"},{"description":" User LogOute|Automation For \n","passed":true,"pending":false,"os":"Linux","browser":{"name":"chrome","version":"61.0.3163.100"},"message":"Passed","screenShotFile":"00a500b5-0037-0023-0071-00da00f40016.png"},{"description":"Open the Application|Automation \n","passed":true,"pending":false,"os":"Linux","browser":{"name":"chrome","version":"63.0.3239.132"},"message":"Passed.","trace":"","screenShotFile":"00940085-001c-0047-0066-005000fe0059.png"},{"description":"Registration|Automation For \n","passed":true,"pending":false,"os":"Linux","browser":{"name":"chrome","version":"63.0.3239.132"},"message":"Passed.","trace":"","screenShotFile":"00f80098-00a3-00be-00b0-005a00ac0030.png"},{"description":"Login|Automation For \n","passed":true,"pending":false,"os":"Linux","browser":{"name":"chrome","version":"63.0.3239.132"},"message":"Passed","screenShotFile":"00d400ba-0005-0096-0032-004a00140008.png"},{"description":" User Delete|Automation For \n","passed":true,"pending":false,"os":"Linux","browser":{"name":"chrome","version":"63.0.3239.132"},"message":"Passed","screenShotFile":"0076009e-00a6-00a7-00c0-007d0069007e.png"},{"description":" User LogOute|Automation For \n","passed":true,"pending":false,"os":"Linux","browser":{"name":"chrome","version":"63.0.3239.132"},"message":"Passed","screenShotFile":"004700a3-00f6-0023-0008-00b90054009a.png"}];
    });